<?php

namespace Drupal\toolbar_manager\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\toolbar_manager\Entity\ToolbarItemSettings;

class ToolbarManagerForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['toolbar_manager.toolbar_items'];
  }

  public function getFormId() {
    return 'toolbar_manager_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $toolbar_items_settings = ToolbarItemSettings::loadMultipleSorted();

    $module_handler = \Drupal::moduleHandler();
    $toolbar_items = $module_handler->invokeAll('toolbar');
    $module_handler->alter('toolbar', $toolbar_items);

    foreach ($toolbar_items as $id => &$toolbar_item) {
      if (isset($toolbar_items_settings[$id])) {
        $toolbar_item['#weight'] = $toolbar_items_settings[$id]->weight;
      }
      if (!isset($toolbar_item['#weight'])) {
        $toolbar_item['#weight'] = 0;
      }
    }

    uasort($toolbar_items, ['\Drupal\Component\Utility\SortArray', 'sortByWeightProperty']);

    $renderer = \Drupal::service('renderer');

    $group_class = 'group-order-weight';

    $items = [];
    foreach ($toolbar_items as $key => $toolbar_item) {
      $enabled = isset($toolbar_items_settings[$key]) ? $toolbar_items_settings[$key]->enabled : TRUE;
      $items[] = [
        'id' => $key,
        'tab' => strip_tags($renderer->render($toolbar_item['#original_tab'])),
        'enabled' => $enabled,
        'weight' => $toolbar_item['#weight'],
        'custom_label' => $toolbar_items_settings[$key]->custom_label,
        'css_classes' => $toolbar_items_settings[$key]->css_classes,
      ];
    }

    // Build table.
    $form['items'] = [
      '#type' => 'table',
      '#caption' => $this->t('Please clear the cache after making changes.'),
      '#footer' => $this->t('Please clear the cache after making changes.'),
      '#header' => [
        $this->t('Tab'),
        $this->t('Default label'),
        $this->t('Custom label'),
        $this->t('CSS classes'),
        $this->t('Enabled'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('No items.'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
          'hidden' => TRUE,
        ]
      ],
    ];

    // Build rows.
    foreach ($items as $key => $value) {
      $form['items'][$key]['#attributes']['class'][] = 'draggable';
      $form['items'][$key]['#weight'] = $value['weight'];

      // ID col.
      $form['items'][$key]['id'] = [
        '#type' => 'item',
        '#value' => $value['id'],
        '#plain_text' => $value['id'],
      ];

      // Label col.
      $form['items'][$key]['tab'] = [
        '#plain_text' => $value['tab'],
      ];

      $form['items'][$key]['custom_label'] = [];
      if (isset($toolbar_items[$value['id']]['tab']['#type']) && in_array($toolbar_items[$value['id']]['tab']['#type'], ['link', 'html_tag'])) {
        $form['items'][$key]['custom_label'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Custom label'),
          '#title_display' => 'invisible',
          '#default_value' => $value['custom_label'],
          '#attributes' => ['size' => 30],
        ];
      }

      $form['items'][$key]['css_classes'] = [
        '#type' => 'textfield',
        '#title' => $this->t('CSS classes'),
        '#title_display' => 'invisible',
        '#default_value' => $value['css_classes'],
      ];

      $form['items'][$key]['enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable @id toolbar item', ['@id' => $value['id']]),
        '#title_display' => 'invisible',
        '#default_value' => $value['enabled'],
      ];

      // Weight col.
      $form['items'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $value['tab']]),
        '#title_display' => 'invisible',
        '#default_value' => $value['weight'],
        '#attributes' => ['class' => [$group_class]],
      ];
    }

    $form['items_help'] = [
      '#markup' => '<p>' . $this->t('<strong>CSS Classes</strong> field will be useful to set or override icon by specifying icon classes. These can be possible values to set an icon:') . '</p>'
        . '<ul>'
        .   '<li>toolbar-icon toolbar-icon-escape-admin</li>'
        .   '<li>toolbar-icon toolbar-icon-help</li>'
        .   '<li>toolbar-icon toolbar-icon-help-main</li>'
        .   '<li>toolbar-icon toolbar-icon-menu</li>'
        .   '<li>toolbar-icon toolbar-icon-system-admin-content</li>'
        .   '<li>toolbar-icon toolbar-icon-system-admin-structure</li>'
        .   '<li>toolbar-icon toolbar-icon-system-themes-page</li>'
        .   '<li>toolbar-icon toolbar-icon-entity-user-collection</li>'
        .   '<li>toolbar-icon toolbar-icon-system-modules-list</li>'
        .   '<li>toolbar-icon toolbar-icon-system-admin-config</li>'
        .   '<li>toolbar-icon toolbar-icon-system-admin-reports</li>'
        .   '<li>toolbar-icon toolbar-icon-user</li>'
        . '</ul>',
    ];

    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('admin_toolbar_tools')){
      $form['items_help']['#markup'] .= '<p>' . $this->t('Item "admin_toolbar_tools" is set by "Admin Toolbar Extra Tools" module to set Drupal 8 icon on tools menu. ');
      if ($module_handler->moduleExists('adminimal_admin_toolbar')) {
        $form['items_help']['#markup'] .= $this->t('However, that icon is overridden by "Adminimal Admin Toolbar" module. So, changes you make on "admin_toolbar_tools" will have no effect.');
      }
      else {
        $form['items_help']['#markup'] .= $this->t('Changing order and setting CSS class will have no effect. Disabling it will remove the icon.');
      }
      $form['items_help']['#markup'] .= '</p>';
    }

    // Form action buttons.
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $items = $form_state->getValue('items');

    foreach ($items as $item) {
      $item_settings = ToolbarItemSettings::loadOrCreate($item['id']);
      $item_settings->enabled = (boolean) $item['enabled'];
      $item_settings->weight = $item['weight'];
      $item_settings->custom_label = trim($item['custom_label']);
      $item_settings->css_classes = trim($item['css_classes']);
      $item_settings->save();
    }

    parent::submitForm($form, $form_state);
  }

}